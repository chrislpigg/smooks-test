<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="no" indent="yes"/>
	<xsl:template name="join" >
		<xsl:param name="valueList" select="''"/>
		<xsl:param name="separator" select="','"/>
		<xsl:for-each select="$valueList">
			<xsl:choose>
				<xsl:when test="position() = 1">
					<xsl:value-of select="."/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($separator, .) "/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/Invoice">
		<Invoice>
			<xsl:apply-templates/>
		</Invoice>
	</xsl:template>

	<xsl:template match="/AdvanceShippingNotice">
		<AdvanceShippingNotice>
			<xsl:apply-templates/>
		</AdvanceShippingNotice>
	</xsl:template>

    <xsl:template match="ItemData/*[starts-with(name(), 'ProductInfo0')]"/>
    <xsl:template match="ItemData/*[starts-with(name(), 'ProductInfo1')]"/>
    <xsl:template match="ItemData/*[starts-with(name(), 'ProductInfo2')]"/>

    <xsl:template match="ItemData/*[starts-with(name(), 'ProductInfoQualifier')]">
        <xsl:param name="PairName" select="concat('ProductInfo', substring(name(), string-length('ProductInfoQualifier') + 1))"/>
        <ProductInfo>
            <xsl:attribute name="type"><xsl:value-of select="."/></xsl:attribute>
            <xsl:value-of select="../*[contains(name(), $PairName)]/text()"/>
        </ProductInfo>
    </xsl:template>

    <xsl:template match="ACK/*[starts-with(name(), 'ProductInfo0')]"/>
    <xsl:template match="ACK/*[starts-with(name(), 'ProductInfo1')]"/>
    <xsl:template match="ACK/*[starts-with(name(), 'ProductInfo2')]"/>

    <xsl:template match="ACK/*[starts-with(name(), 'ProductInfoQualifier')]">
        <xsl:param name="PairName" select="concat('ProductInfo', substring(name(), string-length('ProductInfoQualifier') + 1))"/>
        <ProductInfo>
            <xsl:attribute name="type"><xsl:value-of select="."/></xsl:attribute>
            <xsl:value-of select="../*[contains(name(), $PairName)]/text()"/>
        </ProductInfo>
    </xsl:template>

    <xsl:template match="ItemData/Prices">
		<xsl:choose>
			<xsl:when test="*[contains(./Type/text(), 'NET')]"><xsl:apply-templates select="*[contains(./Type/text(), 'NET')][1]"/></xsl:when>
			<xsl:otherwise><xsl:apply-templates select="*[1]"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="ItemData">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
			<Response>
				<xsl:attribute name="quantity">
					<xsl:choose>
						<xsl:when test="SCH">
							<xsl:value-of select="sum(SCH/Quantity/text())"/>
						</xsl:when>
						<xsl:when test="Prices/PricingInfo">
							<xsl:value-of select="sum(Prices/PricingInfo/Quantity/text())"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>0</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</Response>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
