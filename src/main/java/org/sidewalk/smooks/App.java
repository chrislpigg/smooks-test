package org.sidewalk.smooks;

import org.apache.camel.impl.DefaultCamelContext;

/**
 * Hello world!
 */
public class App {

	/**
	 * @param args -
	 * @throws Exception -
	 */
	public static void main(String[] args) throws Exception {
		DefaultCamelContext camelContext = new DefaultCamelContext();
		camelContext.addRoutes(new MyRouteBuilder());

		camelContext.start();

		Thread.sleep(5000);

		camelContext.stop();
	}
}
