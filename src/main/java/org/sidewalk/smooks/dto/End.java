package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI end segments (IEA, GE, SE)
 */
public class End {

	private int groupCount;
	private int transactionCount;
	private int segmentCount;
	private String controlNumber;

	/**
	 * Get the groupCount
	 *
	 * @return the groupCount
	 */
	@XmlElement(name = "GroupCount")
	public int getGroupCount() {
		return this.groupCount;
	}

	/**
	 * Set the groupCount
	 *
	 * @param groupCount - the groupCount to set
	 */
	public void setGroupCount(int groupCount) {
		this.groupCount = groupCount;
	}

	/**
	 * Get the transactionCount
	 *
	 * @return the transactionCount
	 */
	@XmlElement(name = "TransactionCount")
	public int getTransactionCount() {
		return this.transactionCount;
	}

	/**
	 * Set the transactionCount
	 *
	 * @param transactionCount - the transactionCount to set
	 */
	public void setTransactionCount(int transactionCount) {
		this.transactionCount = transactionCount;
	}

	/**
	 * Get the segmentCount
	 *
	 * @return the segmentCount
	 */
	@XmlElement(name = "SegmentCount")
	public int getSegmentCount() {
		return this.segmentCount;
	}

	/**
	 * Set the segmentCount
	 *
	 * @param segmentCount - the segmentCount to set
	 */
	public void setSegmentCount(int segmentCount) {
		this.segmentCount = segmentCount;
	}

	/**
	 * Get the controlNumber
	 *
	 * @return the controlNumber
	 */
	@XmlElement(name = "ControlNumber")
	public String getControlNumber() {
		return this.controlNumber;
	}

	/**
	 * Set the controlNumber
	 *
	 * @param controlNumber - the controlNumber to set
	 */
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}
}
