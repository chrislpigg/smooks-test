package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * EDI exchange (ISA)
 */
public class Exchange {

	private String authInfoQualifier;
	private String authInfo;
	private String secInfoQualifier;
	private String secInfo;
	private String senderIdQualifier;
	private String senderId;
	private String receiverIdQualifier;
	private String receiverId;
	private String date;
	private String time;
	private String controlStandards;
	private String controlVersion;
	private String controlNumber;
	private String ackReq;
	private String testIndicator;
	private String subElementSelector;
	private List<Group> groups;
	private End end;

	/**
	 * Get the authInfoQualifier
	 *
	 * @return the authInfoQualifier
	 */
	@XmlElement(name = "AuthInfoQualifier")
	public String getAuthInfoQualifier() {
		return this.authInfoQualifier;
	}

	/**
	 * Set the authInfoQualifier
	 *
	 * @param authInfoQualifier - the authInfoQualifier to set
	 */
	public void setAuthInfoQualifier(String authInfoQualifier) {
		this.authInfoQualifier = authInfoQualifier;
	}

	/**
	 * Get the authInfo
	 *
	 * @return the authInfo
	 */
	@XmlElement(name = "AuthInfo")
	public String getAuthInfo() {
		return this.authInfo;
	}

	/**
	 * Set the authInfo
	 *
	 * @param authInfo - the authInfo to set
	 */
	public void setAuthInfo(String authInfo) {
		this.authInfo = authInfo;
	}

	/**
	 * Get the secInfoQualifier
	 *
	 * @return the secInfoQualifier
	 */
	@XmlElement(name = "SecInfoQualifier")
	public String getSecInfoQualifier() {
		return this.secInfoQualifier;
	}

	/**
	 * Set the secInfoQualifier
	 *
	 * @param secInfoQualifier - the secInfoQualifier to set
	 */
	public void setSecInfoQualifier(String secInfoQualifier) {
		this.secInfoQualifier = secInfoQualifier;
	}

	/**
	 * Get the secInfo
	 *
	 * @return the secInfo
	 */
	@XmlElement(name = "SecInfo")
	public String getSecInfo() {
		return this.secInfo;
	}

	/**
	 * Set the secInfo
	 *
	 * @param secInfo - the secInfo to set
	 */
	public void setSecInfo(String secInfo) {
		this.secInfo = secInfo;
	}

	/**
	 * Get the senderIdQualifier
	 *
	 * @return the senderIdQualifier
	 */
	@XmlElement(name = "SenderIDQualifier")
	public String getSenderIdQualifier() {
		return this.senderIdQualifier;
	}

	/**
	 * Set the senderIdQualifier
	 *
	 * @param senderIdQualifier - the senderIdQualifier to set
	 */
	public void setSenderIdQualifier(String senderIdQualifier) {
		this.senderIdQualifier = senderIdQualifier;
	}

	/**
	 * Get the senderId
	 *
	 * @return the senderId
	 */
	@XmlElement(name = "SenderID")
	public String getSenderId() {
		return this.senderId;
	}

	/**
	 * Set the senderId
	 *
	 * @param senderId - the senderId to set
	 */
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	/**
	 * Get the receiverIdQualifier
	 *
	 * @return the receiverIdQualifier
	 */
	@XmlElement(name = "ReceiverIDQualifier")
	public String getReceiverIdQualifier() {
		return this.receiverIdQualifier;
	}

	/**
	 * Set the receiverIdQualifier
	 *
	 * @param receiverIdQualifier - the receiverIdQualifier to set
	 */
	public void setReceiverIdQualifier(String receiverIdQualifier) {
		this.receiverIdQualifier = receiverIdQualifier;
	}

	/**
	 * Get the receiverId
	 *
	 * @return the receiverId
	 */
	@XmlElement(name = "ReceiverID")
	public String getReceiverId() {
		return this.receiverId;
	}

	/**
	 * Set the receiverId
	 *
	 * @param receiverId - the receiverId to set
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	/**
	 * Get the date
	 *
	 * @return the date
	 */
	@XmlElement(name = "Date")
	public String getDate() {
		return this.date;
	}

	/**
	 * Set the date
	 *
	 * @param date - the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Get the time
	 *
	 * @return the time
	 */
	@XmlElement(name = "Time")
	public String getTime() {
		return this.time;
	}

	/**
	 * Set the time
	 *
	 * @param time - the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Get the controlStandards
	 *
	 * @return the controlStandards
	 */
	@XmlElement(name = "ControlStandards")
	public String getControlStandards() {
		return this.controlStandards;
	}

	/**
	 * Set the controlStandards
	 *
	 * @param controlStandards - the controlStandards to set
	 */
	public void setControlStandards(String controlStandards) {
		this.controlStandards = controlStandards;
	}

	/**
	 * Get the controlVersion
	 *
	 * @return the controlVersion
	 */
	@XmlElement(name = "ControlVersion")
	public String getControlVersion() {
		return this.controlVersion;
	}

	/**
	 * Set the controlVersion
	 *
	 * @param controlVersion - the controlVersion to set
	 */
	public void setControlVersion(String controlVersion) {
		this.controlVersion = controlVersion;
	}

	/**
	 * Get the controlNumber
	 *
	 * @return the controlNumber
	 */
	@XmlElement(name = "ControlNumber")
	public String getControlNumber() {
		return this.controlNumber;
	}

	/**
	 * Set the controlNumber
	 *
	 * @param controlNumber - the controlNumber to set
	 */
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}

	/**
	 * Get the ackReq
	 *
	 * @return the ackReq
	 */
	@XmlElement(name = "AckReq")
	public String getAckReq() {
		return this.ackReq;
	}

	/**
	 * Set the ackReq
	 *
	 * @param ackReq - the ackReq to set
	 */
	public void setAckReq(String ackReq) {
		this.ackReq = ackReq;
	}

	/**
	 * Get the testIndicator
	 *
	 * @return the testIndicator
	 */
	@XmlElement(name = "TestIndicator")
	public String getTestIndicator() {
		return this.testIndicator;
	}

	/**
	 * Set the testIndicator
	 *
	 * @param testIndicator - the testIndicator to set
	 */
	public void setTestIndicator(String testIndicator) {
		this.testIndicator = testIndicator;
	}

	/**
	 * Get the subElementSelector
	 *
	 * @return the subElementSelector
	 */
	@XmlElement(name = "SubElementSelector")
	public String getSubElementSelector() {
		return this.subElementSelector;
	}

	/**
	 * Set the subElementSelector
	 *
	 * @param subElementSelector - the subElementSelector to set
	 */
	public void setSubElementSelector(String subElementSelector) {
		this.subElementSelector = subElementSelector;
	}

	/**
	 * Get the groups
	 *
	 * @return the groups
	 */
	@XmlElement(name = "Group")
	public List<Group> getGroups() {
		return this.groups;
	}

	/**
	 * Set the groups
	 *
	 * @param groups - the groups to set
	 */
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	/**
	 * Get the end
	 *
	 * @return the end
	 */
	@XmlElement(name = "ExchangeEnd")
	public End getEnd() {
		return this.end;
	}

	/**
	 * Set the end
	 *
	 * @param end - the end to set
	 */
	public void setEnd(End end) {
		this.end = end;
	}
}
