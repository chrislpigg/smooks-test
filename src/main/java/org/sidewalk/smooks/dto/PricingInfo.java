package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 * EDI CTP
 */
public class PricingInfo {

	private String code;
	private BigDecimal unitPrice;
	private BigDecimal quantity;
	private String units;
	private String multiplierQualifier;
	private BigDecimal multiplier;

	/**
	 * Get the type
	 *
	 * @return the type
	 */
	@XmlElement(name = "Code")
	public String getCode() {
		return this.code;
	}

	/**
	 * Set the type
	 *
	 * @param code - the type to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get the unitPrice
	 *
	 * @return the unitPrice
	 */
	@XmlElement(name = "UnitPrice")
	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	/**
	 * Set the unitPrice
	 *
	 * @param unitPrice - the unitPrice to set
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * Get the quantity
	 *
	 * @return the quantity
	 */
	@XmlElement(name = "Quantity")
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	/**
	 * Set the quantity
	 *
	 * @param quantity - the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Get the units
	 *
	 * @return the units
	 */
	@XmlElement(name = "Units")
	public String getUnits() {
		return this.units;
	}

	/**
	 * Set the units
	 *
	 * @param units - the units to set
	 */
	public void setUnits(String units) {
		this.units = units;
	}

	/**
	 * Get the multiplierQualifier
	 *
	 * @return the multiplierQualifier
	 */
	@XmlElement(name = "MultiplierQualifier")
	public String getMultiplierQualifier() {
		return this.multiplierQualifier;
	}

	/**
	 * Set the multiplierQualifier
	 *
	 * @param multiplierQualifier - the multiplierQualifier to set
	 */
	public void setMultiplierQualifier(String multiplierQualifier) {
		this.multiplierQualifier = multiplierQualifier;
	}

	/**
	 * Get the multiplier
	 *
	 * @return the multiplier
	 */
	@XmlElement(name = "Multiplier")
	public BigDecimal getMultiplier() {
		return this.multiplier;
	}

	/**
	 * Set the multiplier
	 *
	 * @param multiplier - the multiplier to set
	 */
	public void setMultiplier(BigDecimal multiplier) {
		this.multiplier = multiplier;
	}

	/**
	 * @return the final price
	 */
	public BigDecimal getFinalPrice() {
		if ( getUnitPrice() == null ) {
			return null;
		} else if ( getMultiplier() == null ) {
			return getUnitPrice();
		} else {
			return getUnitPrice().multiply(getMultiplier());
		}
	}
}
