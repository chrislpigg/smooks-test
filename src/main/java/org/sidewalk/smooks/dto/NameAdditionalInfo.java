package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI N2
 */
public class NameAdditionalInfo {

	private String name1;

	/**
	 * Get the name1
	 *
	 * @return the name1
	 */
	@XmlElement(name = "Name1")
	public String getName1() {
		return this.name1;
	}

	/**
	 * Set the name1
	 *
	 * @param name1 - the name1 to set
	 */
	public void setName1(String name1) {
		this.name1 = name1;
	}

	/**
	 * Get the name2
	 *
	 * @return the name2
	 */
	@XmlElement(name = "Name2")
	public String getName2() {
		return this.name2;
	}

	/**
	 * Set the name2
	 *
	 * @param name2 - the name2 to set
	 */
	public void setName2(String name2) {
		this.name2 = name2;
	}
	private String name2;
}
