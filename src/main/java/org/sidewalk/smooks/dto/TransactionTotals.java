package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI CTT
 */
public class TransactionTotals {

	private String numberOfLineItem;
	private String hashTotal;

	/**
	 * Get the numberOfLineItem
	 *
	 * @return the numberOfLineItem
	 */
	@XmlElement(name = "NumberOfLineItem")
	public String getNumberOfLineItem() {
		return this.numberOfLineItem;
	}

	/**
	 * Set the numberOfLineItem
	 *
	 * @param numberOfLineItem - the numberOfLineItem to set
	 */
	public void setNumberOfLineItem(String numberOfLineItem) {
		this.numberOfLineItem = numberOfLineItem;
	}

	/**
	 * Get the hashTotal
	 *
	 * @return the hashTotal
	 */
	@XmlElement(name = "HashTotal")
	public String getHashTotal() {
		return this.hashTotal;
	}

	/**
	 * Set the hashTotal
	 *
	 * @param hashTotal - the hashTotal to set
	 */
	public void setHashTotal(String hashTotal) {
		this.hashTotal = hashTotal;
	}
}
