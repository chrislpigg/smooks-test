package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI DTM
 */
public class DateTimeRef {

	private String qualifier;
	private String date;
	private String time;
	private String timezone;
	private String century;

	/**
	 * Get the qualifier
	 *
	 * @return the qualifier
	 */
	@XmlElement(name = "Qualifier")
	public String getQualifier() {
		return this.qualifier;
	}

	/**
	 * Set the qualifier
	 *
	 * @param qualifier - the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * Get the date
	 *
	 * @return the date
	 */
	@XmlElement(name = "Date")
	public String getDate() {
		return this.date;
	}

	/**
	 * Set the date
	 *
	 * @param date - the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Get the time
	 *
	 * @return the time
	 */
	@XmlElement(name = "Time")
	public String getTime() {
		return this.time;
	}

	/**
	 * Set the time
	 *
	 * @param time - the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Get the timezone
	 *
	 * @return the timezone
	 */
	@XmlElement(name = "Timezone")
	public String getTimezone() {
		return this.timezone;
	}

	/**
	 * Set the timezone
	 *
	 * @param timezone - the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	/**
	 * Get the century
	 *
	 * @return the century
	 */
	@XmlElement(name = "Century")
	public String getCentury() {
		return this.century;
	}

	/**
	 * Set the century
	 *
	 * @param century - the century to set
	 */
	public void setCentury(String century) {
		this.century = century;
	}
}
