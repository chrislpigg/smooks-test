package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * EDI transaction (ST)
 */
public class Transaction {

	private String code;
	private String controlNumber;
	private Header header;
	private Currency currency;
	private List<DateTimeRef> dateTimeRefs;
	private List<Name> names;
	private List<ItemData> itemDatas;
	private TransactionTotals transactionTotals;
	private End end;

	/**
	 * Get the code
	 *
	 * @return the code
	 */
	@XmlElement(name = "Code")
	public String getCode() {
		return this.code;
	}

	/**
	 * Set the code
	 *
	 * @param code - the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get the controlNumber
	 *
	 * @return the controlNumber
	 */
	@XmlElement(name = "ControlNumber")
	public String getControlNumber() {
		return this.controlNumber;
	}

	/**
	 * Set the controlNumber
	 *
	 * @param controlNumber - the controlNumber to set
	 */
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}

	/**
	 * Get the header
	 *
	 * @return the header
	 */
	@XmlElement(name = "Header")
	public Header getHeader() {
		return this.header;
	}

	/**
	 * Set the header
	 *
	 * @param header - the header to set
	 */
	public void setHeader(Header header) {
		this.header = header;
	}

	/**
	 * Get the currency
	 *
	 * @return the currency
	 */
	@XmlElement(name = "Currency")
	public Currency getCurrency() {
		return this.currency;
	}

	/**
	 * Set the currency
	 *
	 * @param currency - the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * Get the dateTimeRefs
	 *
	 * @return the dateTimeRefs
	 */
	@XmlElement(name = "DateTimeRef")
	public List<DateTimeRef> getDateTimeRefs() {
		return this.dateTimeRefs;
	}

	/**
	 * Set the dateTimeRefs
	 *
	 * @param dateTimeRefs - the dateTimeRefs to set
	 */
	public void setDateTimeRefs(List<DateTimeRef> dateTimeRefs) {
		this.dateTimeRefs = dateTimeRefs;
	}

	/**
	 * Get the names
	 *
	 * @return the names
	 */
	@XmlElement(name = "Name")
	public List<Name> getNames() {
		return this.names;
	}

	/**
	 * Set the names
	 *
	 * @param names - the names to set
	 */
	public void setNames(List<Name> names) {
		this.names = names;
	}

	/**
	 * Get the itemDatas
	 *
	 * @return the itemDatas
	 */
	@XmlElement(name = "ItemData")
	public List<ItemData> getItemDatas() {
		return this.itemDatas;
	}

	/**
	 * Set the itemDatas
	 *
	 * @param itemDatas - the itemDatas to set
	 */
	public void setItemDatas(List<ItemData> itemDatas) {
		this.itemDatas = itemDatas;
	}

	/**
	 * Get the transactionTotals
	 *
	 * @return the transactionTotals
	 */
	@XmlElement(name = "TransactionTotals")
	public TransactionTotals getTransactionTotals() {
		return this.transactionTotals;
	}

	/**
	 * Set the transactionTotals
	 *
	 * @param transactionTotals - the transactionTotals to set
	 */
	public void setTransactionTotals(TransactionTotals transactionTotals) {
		this.transactionTotals = transactionTotals;
	}

	/**
	 * Get the end
	 *
	 * @return the end
	 */
	@XmlElement(name = "TransactionEnd")
	public End getEnd() {
		return this.end;
	}

	/**
	 * Set the end
	 *
	 * @param end - the end to set
	 */
	public void setEnd(End end) {
		this.end = end;
	}
}
