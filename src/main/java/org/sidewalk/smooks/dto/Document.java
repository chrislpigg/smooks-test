package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * EDI document
 */
@XmlRootElement(name = "Document")
public class Document {

	private List<Exchange> exchanges;

	/**
	 * Get the exchanges
	 *
	 * @return the exchanges
	 */
	@XmlElement(name = "Exchange")
	public List<Exchange> getExchanges() {
		return this.exchanges;
	}

	/**
	 * Set the exchanges
	 *
	 * @param exchanges - the exchanges to set
	 */
	public void setExchanges(List<Exchange> exchanges) {
		this.exchanges = exchanges;
	}
}
