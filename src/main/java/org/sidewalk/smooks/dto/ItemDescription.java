package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI PID
 */
public class ItemDescription {

	private String type;
	private String characteristicCode;
	private String agencyQualifierCode;
	private String productDescriptionCode;
	private String description;

	/**
	 * Get the type
	 *
	 * @return the type
	 */
	@XmlElement(name = "Type")
	public String getType() {
		return this.type;
	}

	/**
	 * Set the type
	 *
	 * @param type - the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Get the characteristicCode
	 *
	 * @return the characteristicCode
	 */
	@XmlElement(name = "CharacteristicCode")
	public String getCharacteristicCode() {
		return this.characteristicCode;
	}

	/**
	 * Set the characteristicCode
	 *
	 * @param characteristicCode - the characteristicCode to set
	 */
	public void setCharacteristicCode(String characteristicCode) {
		this.characteristicCode = characteristicCode;
	}

	/**
	 * Get the agencyQualifierCode
	 *
	 * @return the agencyQualifierCode
	 */
	@XmlElement(name = "AgencyQualifierCode")
	public String getAgencyQualifierCode() {
		return this.agencyQualifierCode;
	}

	/**
	 * Set the agencyQualifierCode
	 *
	 * @param agencyQualifierCode - the agencyQualifierCode to set
	 */
	public void setAgencyQualifierCode(String agencyQualifierCode) {
		this.agencyQualifierCode = agencyQualifierCode;
	}

	/**
	 * Get the productDescriptionCode
	 *
	 * @return the productDescriptionCode
	 */
	@XmlElement(name = "ProductDescriptionCode")
	public String getProductDescriptionCode() {
		return this.productDescriptionCode;
	}

	/**
	 * Set the productDescriptionCode
	 *
	 * @param productDescriptionCode - the productDescriptionCode to set
	 */
	public void setProductDescriptionCode(String productDescriptionCode) {
		this.productDescriptionCode = productDescriptionCode;
	}

	/**
	 * Get the description
	 *
	 * @return the description
	 */
	@XmlElement(name = "Description")
	public String getDescription() {
		return this.description;
	}

	/**
	 * Set the description
	 *
	 * @param description - the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
