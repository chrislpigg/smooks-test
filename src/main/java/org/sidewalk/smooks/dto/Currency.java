package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI currency (CUR)
 */
public class Currency {

	private String entityIdCode;
	private String code;

	/**
	 * Get the entityIdCode
	 *
	 * @return the entityIdCode
	 */
	@XmlElement(name = "EntityIDCode")
	public String getEntityIdCode() {
		return this.entityIdCode;
	}

	/**
	 * Set the entityIdCode
	 *
	 * @param entityIdCode - the entityIdCode to set
	 */
	public void setEntityIdCode(String entityIdCode) {
		this.entityIdCode = entityIdCode;
	}

	/**
	 * Get the code
	 *
	 * @return the code
	 */
	@XmlElement(name = "Code")
	public String getCode() {
		return this.code;
	}

	/**
	 * Set the code
	 *
	 * @param code - the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
}
