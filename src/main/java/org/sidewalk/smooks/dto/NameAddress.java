package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI N3
 */
public class NameAddress {

	private String address1;
	private String address2;
	/**
	 * Get the address1
	 *
	 * @return the address1
	 */
	@XmlElement(name = "Address1")
	public String getAddress1() {
		return this.address1;
	}

	/**
	 * Set the address1
	 *
	 * @param address1 - the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	/**
	 * Get the address2
	 *
	 * @return the address2
	 */
	@XmlElement(name = "Address2")
	public String getAddress2() {
		return this.address2;
	}
	/**
	 * Set the address2
	 *
	 * @param address2 - the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
}
