package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author chrislpigg
 *         Description: ACK - liine item Acknowledgement.  To acknowldege the ordered of quantities and specify the ready date
 *         for a specific line item.
 */
public class ItemAcknowledgement {

	private String statusCode;
	private BigDecimal quantity;
	private String unit;
	private String dateQualifier;
	private String date;
	private List<ItemProductInfo> productInfos;
	private IndustryCode industryCode;

	/**
	 * Description: Code specifying the action taken by the seller on a line item requested by the buyer
	 *
	 * @return String - status code (IA, IQ, IR)
	 */
	@XmlElement(name = "StatusCode")
	public String getStatusCode() {
		return statusCode.toString();
	}

	/**
	 * Setter
	 * Description: Code specifying the action taken by the seller on a line item requested by the buyer
	 *
	 * @param statusCode - status code (IA, IQ, IR)
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Description: numeric value of quantity
	 *
	 * @return BigDecimal - quantity
	 */
	@XmlElement(name = "Quantity")
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Setter..
	 * Description: numeric value of quantity
	 *
	 * @param quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Description: code specifying the units in which a value is being expressed, or manner in which a
	 * measurement has been taken.
	 *
	 * @return String (UN)
	 */
	@XmlElement(name = "Unit")
	public String getUnit() {
		return unit;
	}

	/**
	 * Setter..
	 * Description: code specifying the units in which a value is being expressed, or manner in which a
	 * measurement has been taken.
	 *
	 * @param unit - String (UN)
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Description: Code specifying type of date or time, or both date and time.
	 * 080 - Scheduled for shipment (After and Including)
	 * 100 - No shipping schedule Established as of...
	 *
	 * @return String - Date qualifier
	 */
	@XmlElement(name = "DateQualifier")
	public String getDateQualifier() {
		return dateQualifier;
	}

	/**
	 * Setter..
	 * Description: Code specifying type of date or time, or both date and time.
	 * 080 - Scheduled for shipment (After and Including)
	 * 100 - No shipping schedule Established as of...
	 *
	 * @param dateQualifier
	 */
	public void setDateQualifier(String dateQualifier) {
		this.dateQualifier = dateQualifier;
	}

	/**
	 * Description: (YYMMDD)
	 *
	 * @return String - date
	 */
	@XmlElement(name = "Date")
	public String getDate() {
		return date;
	}

	/**
	 * Setter
	 * Description: (YYMMDD)
	 *
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Description: Code identifying the type/source of the descriptive number used in Product/Service ID (234)
	 * AI	Alternate ISBN
	 * EN     (ISBN-13) – Effectively doubles the number of ISBNs available.
	 * IB	International Standard Book Number (ISBN)
	 * UK     UCC/EAN-14:  Used when a leading digit is added to the ISBN-   13 that specifies the packaging level (such as a carton). When the packaging level is greater than a unit (such as a carton), a new check digit must be calculated.
	 * RR      Alternate EN (ISBN-13)
	 * SR      Alternate UCC/EAN-14
	 *
	 * @return List<ItemProductInfo> - list of Item Product Info
	 */
	@XmlElement(name = "ProductInfo")
	public List<ItemProductInfo> getProductInfos() {
		return this.productInfos;
	}

	/**
	 * Set the productInfos
	 *
	 * @param productInfos - the productInfos to set
	 */
	public void setProductInfos(List<ItemProductInfo> productInfos) {
		this.productInfos = productInfos;
	}

	/**
	 * @return String - IndustryCode
	 */
	@XmlElement(name = "Code")
	public String getCode() {
		return industryCode.toString();
	}

	/**
	 * @param code - IndustryCode
	 */
	public void setCode(String code) {
		IndustryCode industryCode = null;
		try {
			industryCode = IndustryCode.valueOf(code);
		} catch (IllegalArgumentException ex) {
			//found no enum type... do nothing the value should be null
		}
		this.industryCode = industryCode;
	}
}