package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author chrislpigg
 *
 */
@XmlType(name = "IndustryCode")
public enum IndustryCode {

	AC("Item accepted and shipped (Shipping)"),
 	AN("Available:Shipping - new edition/ISBN (Shipping)"),
 	AO("Available: Shipping From Other Location"),
 	AR("Item Accepted: Released for shipment (Shipping)"),
 	AS("Available: Shipping - same ISBN (Shipping)"),
 	AV("Inventory available for order (Available)"),
 	AX("Available : Shipping - free book (Shipping)"),
 	BA("Backordered: Not yet available (Not Shipped)"),
 	BB("Backordered: Reprint under consideration (Not Shipped)"),
 	BC("Backordered: Current edition not available - to be replaced with...(Not Shipped)"),
 	BD("Backordered: Delay in publication (Not Shipped)"),
 	BH("Backordered: On hold (Not Shipped)"),
 	BI("Backordered: To be reissued (Not Shipped)"),
 	BK("Backordered: from previous order (Not Shipped)"),
 	BN("Backordered: Inventory in progress; closed for stocktaking (Not Shipped)"),
 	BO("Backordered: At customer's request (Not Shipped)"),
 	BP("Item Accepted: partial shipment; balance backordered (Shipping)"),
 	BR("Backordered: To be reprinted (Not Shipped)"),
 	BW("Backordered: Waiting for catalog/proessing (Not Shipped)"),
 	BX("Backordered: Not yet published (Not Shipped)"),
 	CA("Cancelled: Not yet available (Not Shipped)"),
 	CB("Cancelled:  Not our publication (Not Shipped)"),
 	CD("Cancelled: Delay in publication (Not Shipped)"),
 	CE("Cancelled: Order partially filled and shipped Description:   remainder of order cancelled (Shipping)"),
 	CF("Cancelled: Currend edition not available (Not Shipped)"),
 	CG("Cancelled: No geographic rights, e.g. Canadian (Not Shipped)"),
 	CH("Cancelled: Rights no longer ours (Not Shipped)"),
 	CI("Cancelled: To be reissued (Not Shipped)"),
 	CJ("Cancelled: Out of print in cloth; available in paper - reorder (Not Shipped)"),
 	CL("Cancelled: Out of print in paper; available in cloth - reorder (Not Shipped)"),
 	CN("Cancelled: Inventory in progress; closed for stocktaking (Not Shipped)"),
 	CO("Cancelled: Out of stock (Not Shipped)"),
 	CQ("Cancelled: Did not meet minimum order requirements Description:   e.g., quantity or dollar value (Not Shipped)"),
 	CR("Cancelled: To be reprinted (Not Shipped)"),
 	CT("Cancelled: Publisher did not respond by your cancellation date (Not Shipped)"),
 	CU("Cancelled: Kits not available (Not Shipped)"),
 	CV("Cancelled: Complete set volume must be purchased (Not Shipped)"),
 	CW("Cancelled: Apply direct; not available through wholesale channels (Not Shipped)"),
 	CX("Cancelled: Never published (Not Shipped)"),
 	CY("Cancelled: Not available as a processed book (Not Shipped)"),
 	DR("Item accepted: Date rescheduled (Shipping)"),
 	DS("Out of Stock"),
 	IA("Item Accepted (Shipping)"),
 	IB("Item Backordered (Not Shipped)"),
 	ID("Item Deleted (Not Shipped)"),
 	IE("Item accepted: Price pending (Shipping)"),
 	IF("Item on hold: Incomplete description (Not Shipped)"),
 	IG("Item Forwarded to Proper Source/Published"),
 	IH("Item on hold (Not Shipped)"),
 	IN("Item accepted: Forwarded to new supplier (Not Shipped)"),
 	IP("Item accepted: Price changed (Shipping)"),
 	IQ("Item accepted: Quantity changed (Shipping)"),
 	IR("Item rejected (Not Shipped)"),
 	IS("Item acepted: Substitution made (Shipping)"),
 	IW("Item on hold: Waiver required (Not Shipped)"),
 	KC("Cancelled: REprint under consideration (Not Shipped)"),
 	KK("Cancelled: ISBN incorrect/unknown (Not Shipped)"),
 	KM("Cancelled: Market for this title is restricted (Not Shipped)"),
 	KP("Cancelled: Out of print (Not Shipped)"),
 	KR("Cancelled: Title Remaindered (Not Shipped)"),
 	KS("Cancelled: book sold by subscription only (Not Shipped)"),
 	NF("Not yet published (Not Shipped)"),
 	OP("Out of print (Not Shipped)"),
 	OR("Temporarily out of stock (Not Shipped)"),
 	PA("Partial shipment (Shipping)"),
 	SC("Shipment complete (Shipping)"),
 	SP("Item accepted: Schedule date pending (Shipping)"),
 	SS("Split Shipment (Shipping)");
	
	private String description;
	
	/**
	 * Private Constructor
	 * @param desc - returns the description of the enumeration.
	 */
	private IndustryCode(String desc) {
		this.description = desc;
	}
	
	/**
	 * 
	 * @return String - description
	 */
	public String getDesc() {
		return this.description;
	}
}