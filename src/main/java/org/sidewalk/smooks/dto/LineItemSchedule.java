package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 * 
 * @author chrislpigg
 *
 */
public class LineItemSchedule {

	private BigDecimal quantity;
	private String unit;
	private String entityIDCode;
	private String name;
	private String dateQualifier;
	private String date;
	private String ReqRefNumber;
	
	/**
	 * 
	 * @return BigDecimal - quantity
	 */
	@XmlElement(name = "Quantity")
	public BigDecimal getQuantity() {
		return quantity;
	}
	
	/**
	 * 
	 * @param quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * 
	 * @return String - unit
	 */
	@XmlElement(name = "Unit")
	public String getUnit() {
		return unit;
	}
	
	/**
	 * 
	 * @param unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	/**
	 * 
	 * @return String - EntityIDCode
	 */
	@XmlElement(name = "EntityIDCode")
	public String getEntityIDCode() {
		return entityIDCode;
	}
	
	/**
	 * 
	 * @param entityIDCode
	 */
	public void setEntityIDCode(String entityIDCode) {
		this.entityIDCode = entityIDCode;
	}
	
	/**
	 * 
	 * @return String - name
	 */
	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return String - DateQualifier
	 */
	@XmlElement(name = "DateQualifier")
	public String getDateQualifier() {
		return dateQualifier;
	}
	
	/**
	 * 
	 * @param dateQualifier
	 */
	public void setDateQualifier(String dateQualifier) {
		this.dateQualifier = dateQualifier;
	}
	
	/**
	 * 
	 * @return String - date
	 */
	@XmlElement(name = "Date")
	public String getDate() {
		return date;
	}
	
	/**
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * 
	 * @return String - ReqRefNumber
	 */
	@XmlElement(name = "ReqRefNumber")
	public String getReqRefNumber() {
		return ReqRefNumber;
	}
	
	/**
	 * 
	 * @param reqRefNumber
	 */
	public void setReqRefNumber(String reqRefNumber) {
		ReqRefNumber = reqRefNumber;
	}
}