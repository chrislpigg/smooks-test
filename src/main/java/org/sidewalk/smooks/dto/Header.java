package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI Header (BIG [810] or BAK [855])
 */
public class Header {

	// Shared
	private String purposeCode;
	private String poNumber;
	private String releaseNumber;
	private String date1;
	private String date2;
	private String date3;

	// 810
	private String invoiceNumber;
	private String changeOrderSequenceNumber;
	private String transactionTypeCode;

	// 855
	private String ackType;
	private String requestReferenceNumber;
	private String contractNumber;

	/**
	 * Get the purposeCode
	 *
	 * @return the purposeCode
	 */
	@XmlElement(name = "PurposeCode")
	public String getPurposeCode() {
		return this.purposeCode;
	}

	/**
	 * Set the purposeCode
	 *
	 * @param purposeCode - the purposeCode to set
	 */
	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}

	/**
	 * Get the poNumber
	 *
	 * @return the poNumber
	 */
	@XmlElement(name = "PONumber")
	public String getPoNumber() {
		return this.poNumber;
	}

	/**
	 * Set the poNumber
	 *
	 * @param poNumber - the poNumber to set
	 */
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	/**
	 * Get the releaseNumber
	 *
	 * @return the releaseNumber
	 */
	@XmlElement(name = "ReleaseNumber")
	public String getReleaseNumber() {
		return this.releaseNumber;
	}

	/**
	 * Set the releaseNumber
	 *
	 * @param releaseNumber - the releaseNumber to set
	 */
	public void setReleaseNumber(String releaseNumber) {
		this.releaseNumber = releaseNumber;
	}

	/**
	 * Get the date1
	 *
	 * @return the date1
	 */
	@XmlElement(name = "Date1")
	public String getDate1() {
		return this.date1;
	}

	/**
	 * Set the date1
	 *
	 * @param date1 - the date1 to set
	 */
	public void setDate1(String date1) {
		this.date1 = date1;
	}

	/**
	 * Get the date2
	 *
	 * @return the date2
	 */
	@XmlElement(name = "Date2")
	public String getDate2() {
		return this.date2;
	}

	/**
	 * Set the date2
	 *
	 * @param date2 - the date2 to set
	 */
	public void setDate2(String date2) {
		this.date2 = date2;
	}

	/**
	 * Get the date3
	 *
	 * @return the date3
	 */
	@XmlElement(name = "Date3")
	public String getDate3() {
		return this.date3;
	}

	/**
	 * Set the date3
	 *
	 * @param date3 - the date3 to set
	 */
	public void setDate3(String date3) {
		this.date3 = date3;
	}

	/**
	 * Get the invoiceNumber
	 *
	 * @return the invoiceNumber
	 */
	@XmlElement(name = "InvoiceNumber")
	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	/**
	 * Set the invoiceNumber
	 *
	 * @param invoiceNumber - the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * Get the changeOrderSequenceNumber
	 *
	 * @return the changeOrderSequenceNumber
	 */
	@XmlElement(name = "ChangeOrderSequenceNumber")
	public String getChangeOrderSequenceNumber() {
		return this.changeOrderSequenceNumber;
	}

	/**
	 * Set the changeOrderSequenceNumber
	 *
	 * @param changeOrderSequenceNumber - the changeOrderSequenceNumber to set
	 */
	public void setChangeOrderSequenceNumber(String changeOrderSequenceNumber) {
		this.changeOrderSequenceNumber = changeOrderSequenceNumber;
	}

	/**
	 * Get the transactionTypeCode
	 *
	 * @return the transactionTypeCode
	 */
	@XmlElement(name = "TransactionTypeCode")
	public String getTransactionTypeCode() {
		return this.transactionTypeCode;
	}

	/**
	 * Set the transactionTypeCode
	 *
	 * @param transactionTypeCode - the transactionTypeCode to set
	 */
	public void setTransactionTypeCode(String transactionTypeCode) {
		this.transactionTypeCode = transactionTypeCode;
	}

	/**
	 * Get the ackType
	 *
	 * @return the ackType
	 */
	@XmlElement(name = "AckType")
	public String getAckType() {
		return this.ackType;
	}

	/**
	 * Set the ackType
	 *
	 * @param ackType - the ackType to set
	 */
	public void setAckType(String ackType) {
		this.ackType = ackType;
	}

	/**
	 * Get the requestReferenceNumber
	 *
	 * @return the requestReferenceNumber
	 */
	@XmlElement(name = "RequestReferenceNumber")
	public String getRequestReferenceNumber() {
		return this.requestReferenceNumber;
	}

	/**
	 * Set the requestReferenceNumber
	 *
	 * @param requestReferenceNumber - the requestReferenceNumber to set
	 */
	public void setRequestReferenceNumber(String requestReferenceNumber) {
		this.requestReferenceNumber = requestReferenceNumber;
	}

	/**
	 * Get the contractNumber
	 *
	 * @return the contractNumber
	 */
	@XmlElement(name = "ContractNumber")
	public String getContractNumber() {
		return this.contractNumber;
	}

	/**
	 * Set the contractNumber
	 *
	 * @param contractNumber - the contractNumber to set
	 */
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
}
