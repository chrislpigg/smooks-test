package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.util.List;

/**
 * EDI PO1/IT1
 */
public class ItemData {

	private String assignedID;
	private BigDecimal quantityInvoiced;
	private String quantityUnit;
	private BigDecimal unitPrice;
	private String unitPriceUnit;
	private List<ItemProductInfo> productInfos;
	private List<PricingInfo> pricingInfos;
	private ItemDescription description;
	private ItemResponse response;
	private ItemAcknowledgement acknowledgement;
	private LineItemSchedule schedule;

	/**
	 * Get the assignedID
	 *
	 * @return the assignedID
	 */
	@XmlElement(name = "AssignedID")
	public String getAssignedID() {
		return this.assignedID;
	}

	/**
	 * Set the assignedID
	 *
	 * @param assignedID - the assignedID to set
	 */
	public void setAssignedID(String assignedID) {
		this.assignedID = assignedID;
	}

	/**
	 * Get the quantityInvoiced
	 *
	 * @return the quantityInvoiced
	 */
	@XmlElement(name = "QuantityInvoiced")
	public BigDecimal getQuantityInvoiced() {
		return this.quantityInvoiced;
	}

	/**
	 * Set the quantityInvoiced
	 *
	 * @param quantityInvoiced - the quantityInvoiced to set
	 */
	public void setQuantityInvoiced(BigDecimal quantityInvoiced) {
		this.quantityInvoiced = quantityInvoiced;
	}

	/**
	 * Get the quantityUnit
	 *
	 * @return the quantityUnit
	 */
	@XmlElement(name = "QuantityUnit")
	public String getQuantityUnit() {
		return this.quantityUnit;
	}

	/**
	 * Set the quantityUnit
	 *
	 * @param quantityUnit - the quantityUnit to set
	 */
	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	/**
	 * Get the unitPrice
	 *
	 * @return the unitPrice
	 */
	@XmlElement(name = "UnitPrice")
	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	/**
	 * Set the unitPrice
	 *
	 * @param unitPrice - the unitPrice to set
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * Get the unitPriceUnit
	 *
	 * @return the unitPriceUnit
	 */
	@XmlElement(name = "UnitPriceUnit")
	public String getUnitPriceUnit() {
		return this.unitPriceUnit;
	}

	/**
	 * Set the unitPriceUnit
	 *
	 * @param unitPriceUnit - the unitPriceUnit to set
	 */
	public void setUnitPriceUnit(String unitPriceUnit) {
		this.unitPriceUnit = unitPriceUnit;
	}

	/**
	 * Get the productInfos
	 *
	 * @return the productInfos
	 */
	@XmlElement(name = "ProductInfo")
	public List<ItemProductInfo> getProductInfos() {
		return this.productInfos;
	}

	/**
	 * Set the productInfos
	 *
	 * @param productInfos - the productInfos to set
	 */
	public void setProductInfos(List<ItemProductInfo> productInfos) {
		this.productInfos = productInfos;
	}

	/**
	 * Get the pricingInfos
	 *
	 * @return the pricingInfos
	 */
	@XmlElement(name = "PricingInfo")
	public List<PricingInfo> getPricingInfos() {
		return this.pricingInfos;
	}

	/**
	 * Set the pricingInfos
	 *
	 * @param pricingInfos - the pricingInfos to set
	 */
	public void setPricingInfos(List<PricingInfo> pricingInfos) {
		this.pricingInfos = pricingInfos;
	}

	/**
	 * Get the description
	 *
	 * @return the description
	 */
	@XmlElement(name = "Description")
	public ItemDescription getDescription() {
		return this.description;
	}

	/**
	 * Set the description
	 *
	 * @param description - the description to set
	 */
	public void setDescription(ItemDescription description) {
		this.description = description;
	}

	/**
	 * Get the response
	 *
	 * @return the response
	 */
	@XmlElement(name = "Response")
	public ItemResponse getResponse() {
		return this.response;
	}

	/**
	 * Set the response
	 *
	 * @param response - the response to set
	 */
	public void setResponse(ItemResponse response) {
		this.response = response;
	}

	/**
	 * 
	 * @return
	 */
	@XmlElement(name = "ACK")
	public ItemAcknowledgement getAcknowledgement() {
		return acknowledgement;
	}
	public void setAcknowledgement(ItemAcknowledgement acknowledgement) {
		this.acknowledgement = acknowledgement;
	}

	@XmlElement(name = "SCH")
	public LineItemSchedule getSchedule() {
		return schedule;
	}
	public void setSchedule(LineItemSchedule schedule) {
		this.schedule = schedule;
	}
}