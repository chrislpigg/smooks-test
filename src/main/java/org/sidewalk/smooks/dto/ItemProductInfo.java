package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Combined EDI stuff
 */
public class ItemProductInfo {

	private String type;
	private String info;

	/**
	 * Get the type
	 *
	 * @return the type
	 */
	@XmlAttribute(name = "type")
	public String getType() {
		return this.type;
	}

	/**
	 * Set the type
	 *
	 * @param type - the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Get the info
	 *
	 * @return the info
	 */
	@XmlValue
	public String getInfo() {
		return this.info;
	}

	/**
	 * Set the info
	 *
	 * @param info - the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}
}
