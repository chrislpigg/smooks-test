package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlAttribute;
import java.math.BigDecimal;

/**
 * Item response (combined EDI segements)
 */
public class ItemResponse {

	private String statusCode;
	private String code;
	private BigDecimal quantity;

	/**
	 * Get the statusCode
	 *
	 * @return the statusCode
	 */
	@XmlAttribute(name = "statusCode")
	public String getStatusCode() {
		return this.statusCode;
	}

	/**
	 * Set the statusCode
	 *
	 * @param statusCode - the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Get the code
	 *
	 * @return the code
	 */
	@XmlAttribute(name = "code")
	public String getCode() {
		return this.code;
	}

	/**
	 * Set the code
	 *
	 * @param code - the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get the quantity
	 *
	 * @return the quantity
	 */
	@XmlAttribute(name = "quantity")
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	/**
	 * Set the quantity
	 *
	 * @param quantity - the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
