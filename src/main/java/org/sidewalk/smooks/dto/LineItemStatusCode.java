package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author chrislpigg
 *
 */
@XmlType(name = "LineItemStatusCode")
public enum LineItemStatusCode {

	IA("Item Accepted"),
	IQ("Item Accepted - Quantity Changed"),
	IR("Item Rejected");
	
	private String description;
	
	/**
	 * Private Constructor
	 * @param desc
	 */
	private LineItemStatusCode(String desc) {
		this.description = desc;
	}
	
	/**
	 * 
	 * @return String - description
	 */
	public String getDesc() {
		return this.description;
	}
}