package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * EDI group (GS)
 */
public class Group {

	private String functionGroup;
	private String appSenderCode;
	private String appReceiverCode;
	private String date;
	private String time;
	private String controlNumber;
	private String responsableAgencyCode;
	private String versionIDCode;
	private List<Transaction> transactions;
	private End end;

	/**
	 * Get the functionGroup
	 *
	 * @return the functionGroup
	 */
	@XmlElement(name = "FunctionGroup")
	public String getFunctionGroup() {
		return this.functionGroup;
	}

	/**
	 * Set the functionGroup
	 *
	 * @param functionGroup - the functionGroup to set
	 */
	public void setFunctionGroup(String functionGroup) {
		this.functionGroup = functionGroup;
	}

	/**
	 * Get the appSenderCode
	 *
	 * @return the appSenderCode
	 */
	@XmlElement(name = "AppSenderCode")
	public String getAppSenderCode() {
		return this.appSenderCode;
	}

	/**
	 * Set the appSenderCode
	 *
	 * @param appSenderCode - the appSenderCode to set
	 */
	public void setAppSenderCode(String appSenderCode) {
		this.appSenderCode = appSenderCode;
	}

	/**
	 * Get the appReceiverCode
	 *
	 * @return the appReceiverCode
	 */
	@XmlElement(name = "AppReceiverCode")
	public String getAppReceiverCode() {
		return this.appReceiverCode;
	}

	/**
	 * Set the appReceiverCode
	 *
	 * @param appReceiverCode - the appReceiverCode to set
	 */
	public void setAppReceiverCode(String appReceiverCode) {
		this.appReceiverCode = appReceiverCode;
	}

	/**
	 * Get the date
	 *
	 * @return the date
	 */
	@XmlElement(name = "Date")
	public String getDate() {
		return this.date;
	}

	/**
	 * Set the date
	 *
	 * @param date - the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Get the time
	 *
	 * @return the time
	 */
	@XmlElement(name = "Time")
	public String getTime() {
		return this.time;
	}

	/**
	 * Set the time
	 *
	 * @param time - the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Get the controlNumber
	 *
	 * @return the controlNumber
	 */
	@XmlElement(name = "ControlNumber")
	public String getControlNumber() {
		return this.controlNumber;
	}

	/**
	 * Set the controlNumber
	 *
	 * @param controlNumber - the controlNumber to set
	 */
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}

	/**
	 * Get the responsableAgencyCode
	 *
	 * @return the responsableAgencyCode
	 */
	@XmlElement(name = "ResponsableAgencyCode")
	public String getResponsableAgencyCode() {
		return this.responsableAgencyCode;
	}

	/**
	 * Set the responsableAgencyCode
	 *
	 * @param responsableAgencyCode - the responsableAgencyCode to set
	 */
	public void setResponsableAgencyCode(String responsableAgencyCode) {
		this.responsableAgencyCode = responsableAgencyCode;
	}

	/**
	 * Get the versionIDCode
	 *
	 * @return the versionIDCode
	 */
	@XmlElement(name = "Date")
	public String getVersionIDCode() {
		return this.versionIDCode;
	}

	/**
	 * Set the versionIDCode
	 *
	 * @param versionIDCode - the versionIDCode to set
	 */
	public void setVersionIDCode(String versionIDCode) {
		this.versionIDCode = versionIDCode;
	}

	/**
	 * Get the transactions
	 *
	 * @return the transactions
	 */
	@XmlElement(name = "Transaction")
	public List<Transaction> getTransactions() {
		return this.transactions;
	}

	/**
	 * Set the transactions
	 *
	 * @param transactions - the transactions to set
	 */
	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	/**
	 * Get the end
	 *
	 * @return the end
	 */
	@XmlElement(name = "GroupEnd")
	public End getEnd() {
		return this.end;
	}

	/**
	 * Set the end
	 *
	 * @param end - the end to set
	 */
	public void setEnd(End end) {
		this.end = end;
	}
}
