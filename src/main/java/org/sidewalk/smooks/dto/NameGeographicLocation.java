package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * EDI N4
 */
public class NameGeographicLocation {

	private String city;
	private String stateProvidence;
	private String postalCode;
	private String countryCode;

	/**
	 * Get the city
	 *
	 * @return the city
	 */
	@XmlElement(name = "City")
	public String getCity() {
		return this.city;
	}

	/**
	 * Set the city
	 *
	 * @param city - the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Get the stateProvidence
	 *
	 * @return the stateProvidence
	 */
	@XmlElement(name = "StateProvidence")
	public String getStateProvidence() {
		return this.stateProvidence;
	}

	/**
	 * Set the stateProvidence
	 *
	 * @param stateProvidence - the stateProvidence to set
	 */
	public void setStateProvidence(String stateProvidence) {
		this.stateProvidence = stateProvidence;
	}

	/**
	 * Get the postalCode
	 *
	 * @return the postalCode
	 */
	@XmlElement(name = "PostalCode")
	public String getPostalCode() {
		return this.postalCode;
	}

	/**
	 * Set the postalCode
	 *
	 * @param postalCode - the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Get the countryCode
	 *
	 * @return the countryCode
	 */
	@XmlElement(name = "CountryCode")
	public String getCountryCode() {
		return this.countryCode;
	}

	/**
	 * Set the countryCode
	 *
	 * @param countryCode - the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
