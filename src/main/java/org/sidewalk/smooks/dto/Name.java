package org.sidewalk.smooks.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * EDI N1
 */
public class Name {

	private String entity;
	private String name;
	private String idCodeQualifier;
	private String idCode;
	private List<NameAdditionalInfo> additionalInfos;
	private List<NameAddress> addresses;
	private NameGeographicLocation geographicLocation;

	/**
	 * Get the entity
	 *
	 * @return the entity
	 */
	@XmlElement(name = "Entity")
	public String getEntity() {
		return this.entity;
	}

	/**
	 * Set the entity
	 *
	 * @param entity - the entity to set
	 */
	public void setEntity(String entity) {
		this.entity = entity;
	}

	/**
	 * Get the name
	 *
	 * @return the name
	 */
	@XmlElement(name = "Name")
	public String getName() {
		return this.name;
	}

	/**
	 * Set the name
	 *
	 * @param name - the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the idCodeQualifier
	 *
	 * @return the idCodeQualifier
	 */
	@XmlElement(name = "IDCodeQualifier")
	public String getIdCodeQualifier() {
		return this.idCodeQualifier;
	}

	/**
	 * Set the idCodeQualifier
	 *
	 * @param idCodeQualifier - the idCodeQualifier to set
	 */
	public void setIdCodeQualifier(String idCodeQualifier) {
		this.idCodeQualifier = idCodeQualifier;
	}

	/**
	 * Get the idCode
	 *
	 * @return the idCode
	 */
	@XmlElement(name = "IDCode")
	public String getIdCode() {
		return this.idCode;
	}

	/**
	 * Set the idCode
	 *
	 * @param idCode - the idCode to set
	 */
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	/**
	 * Get the additionalInfos
	 *
	 * @return the additionalInfos
	 */
	@XmlElement(name = "AdditionalInfo")
	public List<NameAdditionalInfo> getAdditionalInfos() {
		return this.additionalInfos;
	}

	/**
	 * Set the additionalInfos
	 *
	 * @param additionalInfos - the additionalInfos to set
	 */
	public void setAdditionalInfos(List<NameAdditionalInfo> additionalInfos) {
		this.additionalInfos = additionalInfos;
	}

	/**
	 * Get the addresses
	 *
	 * @return the addresses
	 */
	@XmlElement(name = "Address")
	public List<NameAddress> getAddresses() {
		return this.addresses;
	}

	/**
	 * Set the addresses
	 *
	 * @param addresses - the addresses to set
	 */
	public void setAddresses(List<NameAddress> addresses) {
		this.addresses = addresses;
	}

	/**
	 * Get the geographicLocation
	 *
	 * @return the geographicLocation
	 */
	@XmlElement(name = "GeographicLocation")
	public NameGeographicLocation getGeographicLocation() {
		return this.geographicLocation;
	}

	/**
	 * Set the geographicLocation
	 *
	 * @param geographicLocation - the geographicLocation to set
	 */
	public void setGeographicLocation(NameGeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}
}
