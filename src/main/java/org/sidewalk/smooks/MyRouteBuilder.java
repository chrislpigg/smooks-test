package org.sidewalk.smooks;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.sidewalk.smooks.dto.Document;
import org.sidewalk.smooks.dto.Exchange;
import org.sidewalk.smooks.dto.Group;
import org.sidewalk.smooks.dto.ItemData;
import org.sidewalk.smooks.dto.ItemProductInfo;
import org.sidewalk.smooks.dto.Transaction;
import org.slf4j.LoggerFactory;

/***/
public class MyRouteBuilder extends RouteBuilder {

	/**
	 * @see org.apache.camel.builder.RouteBuilder#configure()
	 */
	@Override
	public void configure() {
		LoggerFactory.getLogger(MyRouteBuilder.class).info("Route builder");

		JaxbDataFormat jaxb = new JaxbDataFormat("org.sidewalk.smooks.dto");

		from("file:input/?noop=true").convertBodyTo(String.class)
			.to("smooks://edi/pubnet/810_855.convert.xml")
			.to("xslt:edi/pubnet/cleanup.xsl").unmarshal(jaxb)
			.to("file:output/?noop=true");
//			.process(new Processor() {
//
//			@Override
//			public void process(org.apache.camel.Exchange camelExchange) {
//				Document document = camelExchange.getIn().getBody(Document.class);
//
//				for (Exchange exchange : document.getExchanges()) {
//					System.out.println(exchange.getControlNumber());
//					for (Group group : exchange.getGroups()) {
//						System.out.println("\t" + group.getFunctionGroup());
//						for (Transaction transaction : group.getTransactions()) {
//							System.out.println("\t\t" + transaction.getCode());
//							for (ItemData itemData : transaction.getItemDatas()) {
//								System.out.println("\t\t\t" + itemData.getAssignedID());
//								for (ItemProductInfo info : itemData.getProductInfos()) {
//									if ( info.getType().equals("EN") )
//										System.out.println("\t\t\t\t" + info.getInfo());
//								}
//								System.out.println("\t\t\t\t" + itemData.getAcknowledgement().getQuantity());
//							}
//						}
//					}
//				}
//			}
//		});
	}

}
